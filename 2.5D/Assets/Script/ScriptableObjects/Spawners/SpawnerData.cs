﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Spawner.asset", menuName = "Spawners/Spawner")]
[System.Serializable]
public class SpawnerData : ScriptableObject
{
    public int minSpawn;
    public int maxSpawn;
}
