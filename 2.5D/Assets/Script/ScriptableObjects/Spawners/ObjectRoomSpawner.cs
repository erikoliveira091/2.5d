using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Spawnable
{
    public GameObject prefab;

    [Range(0f, 100f)] public float chance = 100f;

    [HideInInspector] public double weight;
}

public class ObjectRoomSpawner : MonoBehaviour
{
    [System.Serializable]
    public struct RandomSpawner
    {
        public string name;
        public SpawnerData spawnerData;
    }

    [SerializeField] private Spawnable[] spawner;

    private double accumulatedWeights;
    private System.Random rand = new System.Random();

    public GridController grid;
    public RandomSpawner[] spawnerData;


    public void InitialiseObjectSpawning()
    {
        foreach (RandomSpawner rs in spawnerData)
        {
            int randoInteration = Random.Range(rs.spawnerData.minSpawn, rs.spawnerData.maxSpawn + 1);
            for (int i = 0; i <= randoInteration; i++)
            {
                SpawnRandomEnemy();
            }
        }
    }

    private void SpawnRandomEnemy()
    {
        

        //Instantiate(randomEnemy.prefab, transform.position, Quaternion.identity, transform);

        int randomPos = Random.Range(0, grid.availablePoints.Count - 1);

        CalculateWeights();

        Spawnable randomEnemy = spawner[GetRandomEnemyIndex()];

        GameObject go = Instantiate(randomEnemy.prefab, grid.availablePoints[randomPos], Quaternion.identity, transform) as GameObject;
        grid.availablePoints.RemoveAt(randomPos);
        Debug.Log("Spawnerd Enemy!");

    }


    private int GetRandomEnemyIndex()
    {
        double r = rand.NextDouble() * accumulatedWeights;

        for (int i = 0; i <= spawner.Length; i++)
            if (spawner[i].weight >= r)
            { 
                return i;
            }
           


        return 0;
    }

    private void CalculateWeights()
    {
        accumulatedWeights = 0f;
        foreach (Spawnable spawn in spawner)
        {
            accumulatedWeights += spawn.chance;
            spawn.weight = accumulatedWeights;
        }
    }
}
