using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletProjectile : MonoBehaviour
{
    private Rigidbody bulletRigibody;
    [SerializeField]
    private float timeToDestroy = 5f;


    private void Awake()
    {
        bulletRigibody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        float speed =  8f;
        bulletRigibody.velocity = transform.forward * speed;
        StartCoroutine(DestroyBullet());
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Enemy"))
        {
            other.GetComponent<Enemy>().Death();
            //Destroy(gameObject);
        }
        
    }
    
    IEnumerator DestroyBullet()
    {
        yield return new WaitForSeconds(timeToDestroy);
        Destroy(gameObject);
    }

}
