using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceCamera : MonoBehaviour
{
    Camera m_camera;
    public Transform mPlayer;
    

    void Awake()
    {
        m_camera = Camera.main;

    }

    void Update()
    {
        
   
         Vector3 targetVector = this.transform.position - m_camera.transform.position;
         transform.rotation = Quaternion.LookRotation(targetVector, m_camera.transform.rotation * new Vector3(0f, 1f, 0f));
    }

    public void DestroyInTime(float timeAlive)
    {
        StartCoroutine(DestroyIE(timeAlive));
    }

    IEnumerator DestroyIE(float timeAlive)
    {
        yield return new WaitForSeconds(timeAlive);
        Destroy(gameObject);
    }
}
