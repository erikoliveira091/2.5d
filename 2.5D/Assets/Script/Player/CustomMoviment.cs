using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
using UnityEngine.InputSystem;
#endif

public class CustomMoviment : MonoBehaviour
{
    public GameObject aimCamera;

    void Start()
    {
        aimCamera.SetActive(false);
    }
  }
