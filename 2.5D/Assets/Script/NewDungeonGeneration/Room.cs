using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    public float Width;

    public float Height;

    public float X;

    public float Y;

    public float Z;

    private bool updatedDoors = false;
    bool updatedEnemies = false;

    public Room(float x, float y, float z)
    {
        X = x;
        Y = y;
        Z = z;
    }

    public Door leftDoor;
    public Door rightDoor;
    public Door topDoor;
    public Door bottomDoor;

    public List<Door> doors = new List<Door>();

    void Start()
    {
        if(RoomController.instance == null)
        {
            Debug.Log("You pressed play int he wrong scene!");
            return;
        }


        Door[] ds = GetComponentsInChildren<Door>();
        foreach (Door d in ds)
        {
            doors.Add(d);
            switch (d.doorType)
            {
                case Door.DoorType.right:
                    rightDoor = d;
                    break;
                case Door.DoorType.left:
                    leftDoor = d;
                    break;
                case Door.DoorType.top:
                    topDoor = d;
                    break;
                case Door.DoorType.bottom:
                    bottomDoor = d;
                    break;
            }
        }

        RoomController.instance.RegisterRoom(this);
    }

    void Update()
    {
        if (name.Contains("End") && !updatedDoors)
        {
            RemoveUnconnectedDoors();
            updatedDoors = true;
        }
    }

    public void RemoveUnconnectedDoors()
    {
        Debug.Log("removing doors");
        foreach (Door door in doors)
        {
            switch (door.doorType)
            {
                case Door.DoorType.right:
                    if (GetRight() == null)
                    { 
                        door.gameObject.SetActive(false);
                       
                    }
                    break;
                case Door.DoorType.left:
                    if (GetLeft() == null)
                    { 
                        door.gameObject.SetActive(false);
                       
                    }
                    break;
                case Door.DoorType.top:
                    if (GetTop() == null)
                    { 
                        door.gameObject.SetActive(false);
                       
                    }
                    break;
                case Door.DoorType.bottom:
                    if (GetBottom() == null) 
                    { 
                        door.gameObject.SetActive(false);
                        
                    }
                    break;
                    
            }

            switch (door.doorType)
            {
                case Door.DoorType.right:
                    if (GetRight() != null)
                    {
                        
                        door.removeWall = true;
                    }
                    break;
                case Door.DoorType.left:
                    if (GetLeft() != null)
                    {
                        
                        door.removeWall = true;

                    }
                    break;
                case Door.DoorType.top:
                    if (GetTop() != null)
                    {
                        
                        door.removeWall = true;

                    }
                    break;
                case Door.DoorType.bottom:
                    if (GetBottom() != null)
                    {
                        
                        door.removeWall = true;

                    }
                    break;

            }


        }
        
    }

    public Room GetRight()
    {
        if(RoomController.instance.DoesRoomExist(X + 1, Y, Z))
        {
            return RoomController.instance.FindRoom(X + 1, Y, Z);
        }
        return null;
    }
    public Room GetLeft()
    {
        if(RoomController.instance.DoesRoomExist(X - 1, Y, Z))
        {
            return RoomController.instance.FindRoom(X - 1, Y, Z);
        }
        return null;
    }
    public Room GetTop()
    {
        if(RoomController.instance.DoesRoomExist(X , Y, Z + 1))
        {
            return RoomController.instance.FindRoom(X, Y, Z + 1);
        }
        return null;
    }
    public Room GetBottom()
    {
        if(RoomController.instance.DoesRoomExist(X , Y, Z - 1))
        {
            return RoomController.instance.FindRoom(X, Y, Z - 1);
        }
        return null;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, new Vector3(Width, Y, Height));
    }

    public Vector3 GetRoomCentre()
    {
        return new Vector3(X * Width ,  Y, Z * Height);
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            if (!updatedEnemies)
            {
                GetComponentInChildren<GridController>().AwakeGrid();
                updatedEnemies = true;
                RoomController.instance.OnPlayerEnterRoom(this);
            }
            
        }
    }

}
