using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonGerator : MonoBehaviour
{
    public DungeonGenerationData dungeonGenerationData;

    private List<Vector3Int> dungeonRooms;

    private void Start()
    {
        dungeonRooms = DungeonCrawlerController.GenerateDungeon(dungeonGenerationData);
        SpawnRooms(dungeonRooms);

    }

    private void SpawnRooms(IEnumerable<Vector3Int> rooms)
    {
        RoomController.instance.LoadRoom("Start", 0, 0, 0);
        foreach(Vector3Int roomLocation in rooms)
        {
            
            RoomController.instance.LoadRoom(RoomController.instance.GetRandomRoomName(), roomLocation.x, 0, roomLocation.z);
                 
        }
    }
}
