using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public enum DoorType
    {
        left, right, top, bottom
    }

    public DoorType doorType;

    public GameObject doorCollider;

    public bool removeWall;

    private GameObject player;
    private float widthOffset = 3f;

    private void Start()
    {
        removeWall = false;
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void OnTriggerStay(Collider other)
    {
        
        if (removeWall == true)
        {
            if(other.CompareTag("Wall"))
            {
                Destroy(other.gameObject);
            }
        }

    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.gameObject.tag == ("Player"))
        {
            switch (doorType)
            {
                case Door.DoorType.right:

                    Debug.Log("Direita");
                    hit.gameObject.transform.position = new Vector3(hit.gameObject.transform.position.x + widthOffset, 0f, hit.gameObject.transform.position.z);

                    break;
                case Door.DoorType.left:

                    Debug.Log("Esquerda");
                    hit.gameObject.transform.position = new Vector3(hit.gameObject.transform.position.x - widthOffset, 0f, hit.gameObject.transform.position.z);

                    break;
                case Door.DoorType.top:

                    Debug.Log("Frente");
                    hit.gameObject.transform.position = new Vector3(hit.gameObject.transform.position.x, 0f, hit.gameObject.transform.position.z + widthOffset);

                    break;
                case Door.DoorType.bottom:

                    Debug.Log("Tr�s");
                    hit.gameObject.transform.position = new Vector3(transform.position.x, 0f, hit.gameObject.transform.position.z - widthOffset);

                    break;
            }
        }

       
    }

    void OnTriggerEnter(Collider other)
    {
        

    }

}


