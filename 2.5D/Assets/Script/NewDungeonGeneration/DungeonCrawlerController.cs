using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    forward = 0,

    left = 1,

    back = 2,

    right = 3
};

public class DungeonCrawlerController : MonoBehaviour
{

    public static List<Vector3Int> positionVisited = new List<Vector3Int>();

    private static readonly Dictionary<Direction, Vector3Int> directionMovementMap = new Dictionary<Direction, Vector3Int>
    {
        {Direction.forward, Vector3Int.forward },
        {Direction.left, Vector3Int.left },
        {Direction.back, Vector3Int.back },
        {Direction.right, Vector3Int.right }
    };

    public static List<Vector3Int> GenerateDungeon(DungeonGenerationData dungeonData)
    {
        List<DungeonCrawler> dungeonCrawlers = new List<DungeonCrawler>();

        for(int i = 0; i < dungeonData.numberOfCrawlers; i++)
        {
            dungeonCrawlers.Add(new DungeonCrawler(Vector3Int.zero));
        }

        int iterations = Random.Range(dungeonData.iterationMin, dungeonData.iterationMax);

        for(int i = 0; i < iterations; i++)
        {
            foreach(DungeonCrawler dungeonCrawler in dungeonCrawlers)
            {
                Vector3Int newPos = dungeonCrawler.Move(directionMovementMap);
                positionVisited.Add(newPos);
            }
        }

        return positionVisited;
    }
}
