using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPatrol : MonoBehaviour
{
    public float speed;
    public Transform[] patrolPoints;
    public float waitTime;
    int currentPointIndex;

    public Transform target;
    public float miniDistance;

    public GameObject projectile;
    public GameObject lacaio;
    public float timeBetweenShots;
    private float nextShotTime;

    [SerializeField]
    private Transform pivotPower;
    [SerializeField]
    private Transform[] pivotLacaio;

    bool once;
    private void Update()
    {
        if(Time.time > nextShotTime)
        {
            Instantiate(projectile, pivotPower.position, Quaternion.identity);
            nextShotTime = Time.time + timeBetweenShots;

          


        }

        if(Vector3.Distance(transform.position, target.position) < miniDistance)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, -speed * Time.deltaTime);
        }

        if(transform.position != patrolPoints[currentPointIndex].position)
        {
            transform.position = Vector3.MoveTowards(transform.position, patrolPoints[currentPointIndex].position, speed * Time.deltaTime);
        }
        else
        {
            if(once == false)
            {
                once = true;
                StartCoroutine(Wait());
            }
        }
    }

    
    IEnumerator Wait()
    {
        yield return new WaitForSeconds(waitTime);
        if(currentPointIndex + 1 < patrolPoints.Length)
        {
            currentPointIndex++;
        }
        else
        {
            currentPointIndex = 0;
        }
        once = false;
    }

    
}
