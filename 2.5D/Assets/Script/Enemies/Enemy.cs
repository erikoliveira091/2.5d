using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyState
{
    Idle,
    Wander,
    Follow,
    Die,
    Attack
};

public enum EnemyType
{
    Melee,
    Ranged
};

public class Enemy : MonoBehaviour
{
    GameObject player;
    public EnemyState currState = EnemyState.Idle;
    public EnemyType enemyType;
    public float range;
    public float speed;
    public float attackRange;
    public float bulletSpeed;
    public float coolDown;
    private bool chooseDir = false;
    private bool dead = false;
    private bool coolDownAttack = false;
    public bool notInRoom = false;
    private Vector3 randomDir;
    public GameObject bulletPrefab;
    [SerializeField] bool takeDamage;
    [SerializeField] Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        switch (currState)
        {
            //case(EnemyState.Idle):
            //    Idle();
            //break;
            case (EnemyState.Wander):
                Wander();
                break;
            case (EnemyState.Follow):
                Follow();
                break;
            case (EnemyState.Die):
                break;
            case (EnemyState.Attack):
                //Attack();
                break;
        }

        if (!notInRoom)
        {
            if (IsPlayerInRange(range) && currState != EnemyState.Die)
            {
                currState = EnemyState.Follow;
            }
            else if (!IsPlayerInRange(range) && currState != EnemyState.Die)
            {
                currState = EnemyState.Wander;
            }
            if (Vector3.Distance(transform.position, player.transform.position) <= attackRange)
            {
                currState = EnemyState.Attack;
            }
        }
        else
        {
            currState = EnemyState.Idle;

        }
    }

    private bool IsPlayerInRange(float range)
    {
        return Vector3.Distance(transform.position, player.transform.position) <= range;
    }

    private IEnumerator ChooseDirection()
    {
        chooseDir = true;
        yield return new WaitForSeconds(Random.Range(2f, 8f));
        randomDir = new Vector3(0, Random.Range(0, 360), 0);
        Quaternion nextRotation = Quaternion.Euler(randomDir);
        transform.rotation = Quaternion.Lerp(transform.rotation, nextRotation, Random.Range(0.5f, 2.5f));
        chooseDir = false;
    }

    void Wander()
    {
        if (!chooseDir)
        {
            StartCoroutine(ChooseDirection());
        }

        transform.position += -transform.right * speed * Time.deltaTime;
        if (IsPlayerInRange(range))
        {
            currState = EnemyState.Follow;
        }
    }

    void Follow()
    {
        transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
    }

    void Attack()
    {
        if (!coolDownAttack)
        {
            switch (enemyType)
            {
                case (EnemyType.Melee):
                    Debug.Log("T� funcionando");
                   
                    //StartCoroutine(CoolDown());
                    break;
                case (EnemyType.Ranged):
                    GameObject bullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity) as GameObject;
                    bullet.GetComponent<Project>().GetPlayer(player.transform);
                    bullet.GetComponent<Project>().isEnemyBullet = true;
                    //StartCoroutine(CoolDown());
                    break;
            }
        }
    }


    public void Death()
    {
        RoomController.instance.StartCoroutine(RoomController.instance.RoomCoroutine());
        Destroy(gameObject);
    }

    void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Health health = other.GetComponent<Health>();
            StartCoroutine(TakeDamage(health, coolDown));
            
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            anim.SetBool("Attack", false);
        }

    }

    private IEnumerator TakeDamage(Health health, float time)
    {
        coolDownAttack = false;
        if (takeDamage)
        {
            anim.SetBool("Attack", takeDamage);
            health.RemoveHearth(); 
            takeDamage = !takeDamage;
            yield return new WaitForSeconds(0.001f);
            anim.SetBool("Attack", takeDamage);
            yield return new WaitForSeconds(time);
            takeDamage = !takeDamage;
        }
        coolDownAttack = true;

    }


}


/*
private Transform target;
public GameObject particle;
public int life;
public float speedEn;
public float stoppingDistance;
public float visao;


// Start is called before the first frame update
void Start()
{
    life = 30;
    target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
}

// Update is called once per frame
void Update()
{ 
    if (Vector2.Distance(transform.position, target.position) > stoppingDistance) {
        if (Vector2.Distance(transform.position, target.position) <= (visao))
            transform.position = Vector2.MoveTowards(transform.position, target.position, speedEn * Time.deltaTime);
    }
    if (life<=0)
    {
        Instantiate(particle, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}

private void OnTriggerEnter2D(Collider2D collision)
{
    if (collision.CompareTag("bullet"))
    {
        life -= 10;
        Destroy(collision.gameObject);
    }
}

}*/