using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [System.Serializable]
    public struct SpawnableEnemy
    {
        public GameObject gameObject;
        public float weight;
    }

    public List<SpawnableEnemy> enemy = new List<SpawnableEnemy>();
    public List<GameObject> loadedEnemies = new List<GameObject>();
    float totalWeight;

    void Awake()
    {
        totalWeight = 0;
        foreach (var spawnable in enemy)
        {
            totalWeight += spawnable.weight;
        }
    }

    // Start is called before the first frame update
    void Start()
    {

        float pick = Random.value * totalWeight;
        int chosenIndex = 0;
        float cumulativeWeight = enemy[0].weight;

        while (pick > cumulativeWeight && chosenIndex < enemy.Count - 1)
        {
            chosenIndex++;
            cumulativeWeight += enemy[chosenIndex].weight;
        }

        GameObject i = Instantiate(enemy[chosenIndex].gameObject, transform.position, Quaternion.identity, transform) as GameObject;
        loadedEnemies.Add(i);

        //GameObject go = Instantiate(data.spawnerData.itemToSpawn, grid.availablePoints[randomPos], Quaternion.identity, transform) as GameObject;

    }
}
